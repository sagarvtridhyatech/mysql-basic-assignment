/* 1.Create database using query */
CREATE DATABASE assi_1;
use assi_1;

/*2.Create table using query (create 2 table members and moviews*/
CREATE TABLE member(

    membership_number int(3) primary key,
    full_name varchar(100),
    gender varchar(100),
    Date_of_Birth date,
    physical_address varchar(255),
    postal_address varchar(255),
    contact_no bigint,
    emial varchar(100)

);
     
    


INSERT INTO `member`(`membership_number`, `full_name`, `gender`, `Date_of_Birth`, `physical_address`, `postal_address`, `contact_no`, `emial`) VALUES (1,'janet jones','female','1980-07-21','First street plot no.4','privat bag',759253542,'janetjones@yahoo.com')


INSERT INTO `member`(`membership_number`, `full_name`, `gender`, `Date_of_Birth`, `physical_address`, `postal_address`, `contact_no`, `emial`) VALUES (2,'janet smith jones','female','1980-06-23','melrose 123',NULL,NULL,'jjf@street.com')

INSERT INTO `member`(`membership_number`, `full_name`, `gender`, `Date_of_Birth`, `physical_address`, `postal_address`, `contact_no`, `emial`) VALUES (3,'Robert Phil','male','1989-07-12','3rd street 34',NULL,12345,'rm@street.com')

INSERT INTO `member`(`membership_number`, `full_name`, `gender`, `Date_of_Birth`, `physical_address`, `postal_address`, `contact_no`, `emial`) VALUES (4,'Gloria Williams','female','1984-02-14','2nd street23',NULL,NULL,NULL)


 use assi_1;

CREATE TABLE movies(

    movie_id int(3) primary key,
    title varchar(50),
    director varchar(20),
    year_released int(4),
    category_id int(3)
);

/*5.Create insert query*/
INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (1,'Pirates of carbian 4','Rob Marshall',2011,1)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (2,'Forgrtting Sarah Marshal','Nicholas Stoller',2008,2)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (3,'X-men',NULL,2008,2)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (4,'Code Name Black','Edgar Jimz',2010,NULL)

SELECT * FROM `movies`

/*3.Create different-different Select query with DISTINCT,*,FROM, WHERE,GROUP BY,HAVING and HAVING*/

SELECT * FROM `movies` WHERE year_released=2007

SELECT * FROM `movies` WHERE category_id=6

SELECT title, year_released FROM 'movies' GROUP BY year_released 
SELECT movie_id, year_released FROM `movies` GROUP BY year_released HAVING movie_id > 4

SELECT DISTINCT year_released FROM `movies` 

/*4.Create query using WHERE Clause(IN, OR, AND, Not IN,Equal To,Not Equal To, Greater than, less than )*/
SELECT * FROM `movies` WHERE year_released=2007 AND category_id=6

SELECT * FROM `movies` WHERE year_released=2007 OR category_id=8

SELECT * FROM `movies` WHERE movie_id IN('2','4','5')

SELECT * FROM `movies` WHERE movie_id NOT IN('2','4','5')

SELECT * FROM `movies` WHERE movie_id=3

SELECT * FROM `movies` WHERE movie_id!=3

SELECT * FROM `movies` WHERE movie_id>4

SELECT * FROM `movies` WHERE movie_id<4

/*6.Get record form members table using select query*/
SELECT * FROM member

/*7.Getting only the full_names, gender, physical_address and email fields only from memeber table*/
SELECT full_name, gender, physical_address, emial FROM member

/*8.Get a member's personal details from members table given the membership number 1*/
SELECT * FROM `member` WHERE membership_number=1

/*9.Get a list of all the movies in category 2 that were released in 2008*/
SELECT title FROM `movies` WHERE category_id=2 AND year_released=2008

/*10.Gets all the movies in either category 1 or category 2*/
SELECT title FROM `movies` WHERE category_id=1 OR category_id=2

/*11.Gets rows where membership_number is either 1 , 2 or 3*/
SELECT * FROM `member` WHERE membership_number=1 OR membership_number=2 OR membership_number=3

/*12.Gets all the female members from the members table using the equal to comparison operator.*/
SELECT full_name FROM `member` WHERE gender=female

/*13.Gets all the payments that are greater than 2,000 from the payments table*/

SELECT * FROM `payments` WHERE payment>2000

